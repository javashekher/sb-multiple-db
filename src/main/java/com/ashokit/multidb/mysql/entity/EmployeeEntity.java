package com.ashokit.multidb.mysql.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity
@Table(name = "EMP")
@Data
public class EmployeeEntity {
	@NotNull
	@Id
	private Integer empno;
	
	@NotEmpty
	private String ename;
	
	@NotNull
	private Double sal;
	
	@NotNull
	private Integer deptno;
	

}
